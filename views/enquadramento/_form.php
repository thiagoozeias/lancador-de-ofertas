<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Enquadramento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enquadramento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'enquadramento')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Salvar enquadramento', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
