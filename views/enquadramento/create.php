<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Enquadramento */

$this->title = 'Novo Enquadramento';
$this->params['breadcrumbs'][] = ['label' => 'Enquadramentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquadramento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
