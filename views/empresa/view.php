<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="empresa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id, 'enquadramento_id' => $model->enquadramento_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['Excluir', 'id' => $model->id, 'enquadramento_id' => $model->enquadramento_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja excluir esse item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [// the owner name of the model
                'attribute' => 'enquadramento_id',
                'value' => $model->enquadramento->enquadramento,
            ],
            'nome',
            'cidade',
            'cnpj',
        ],
    ])
    ?>

</div>
