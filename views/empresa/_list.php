<?php
if (isset($empresas)) {
    foreach ($empresas as $empresa) {
        ?>
        <div class="col-md-3 text-center">
            <div class="panel panel-success panel-empresa">
                <div class="panel-heading">
                    <i class="fa fa-desktop"></i>
                    <h3><?= $empresa->nome ?></h3>
                </div>
                <ul class="list-group text-left">
                    <li class="list-group-item"><i class="fa fa-check"></i><strong>Enquadramento: </strong> <?= $empresa->enquadramento->enquadramento ?></li>
                    <li class="list-group-item"><i class="fa fa-check"></i><strong>CNPJ: </strong> <?= $empresa->cnpj ?></li>
                    <li class="list-group-item"><i class="fa fa-check"></i><strong>Cidade: </strong> <?= $empresa->cidade ?></li>
                </ul>
            </div>
        </div>
        <?php
    }
}


