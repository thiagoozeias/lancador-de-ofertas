<?php
/* @var $this yii\web\View */

$this->title = 'Teste Lançador de Ofertas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Lançador de Ofertas!</h1>

        <p class="lead">Pesquise nos campos abaixo:</p>


    </div>

    <div class="body-content">
        <form id='frmSearch' action='<?= yii\helpers\Url::toRoute(['site/search']) ?>' method="post" onsubmit="enviaDados(this); return false;">
            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>" />
            <div class="row">
                <div class="col-md-3">
                    <label for="enquadramento">
                        Enquadramento
                    </label>
                    <select id="enquadramento" name="Empresa[enquadramento_id]" class="form-control">
                        <option value=''></option>
                        <?php
                        if (isset($enquadramento)) {
                            foreach ($enquadramento as $enqd) {
                                ?>
                                <option value="<?= $enqd->id ?>"><?= $enqd->enquadramento ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="empresa">
                        Empresa
                    </label>
                    <input type="text" name="Empresa[nome]" id="empresa" class="form-control"/>
                </div>
                <div class="col-md-3">
                    <label for="cidade">
                        Cidade
                    </label>
                    <input type="text" name="Empresa[cidade]" id="cidade" class="form-control"/>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-success btn-search form-control"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
        <br/>
        <div class="row" id='response'>
            <?= $empresas ?>
        </div>
    </div>
</div>
<script>

    function enviaDados(element) {
        let form = $("#" + element.id);
        let action = form.attr('action');
        let data = form.serialize();
        $.ajax({
            url: action,
            type: "POST",
            data: data,
            dataType: "html",
            beforeSend: function (xhr) {
                $("#response").fadeOut('slow');
            }
        }).done(function (resposta) {
            $("#response").fadeIn('slow').html(resposta);
        }).fail(function (jqXHR, textStatus) {
            console.log("Request failed: " + textStatus);
        }).always(function () {
            console.log("completou");
        });
    }
</script>
