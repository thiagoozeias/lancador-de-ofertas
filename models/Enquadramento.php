<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "enquadramento".
 *
 * @property int $id
 * @property string|null $enquadramento
 * @property string|null $data
 *
 * @property Empresa[] $empresas
 */
class Enquadramento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enquadramento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['enquadramento'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'enquadramento' => 'Enquadramento',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['enquadramento_id' => 'id']);
    }
}
