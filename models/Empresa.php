<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id
 * @property int $enquadramento_id
 * @property string|null $nome
 * @property string|null $cidade
 * @property string|null $cnpj
 * @property string|null $data
 *
 * @property Enquadramento $enquadramento
 */
class Empresa extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['enquadramento_id'], 'required'],
            [['enquadramento_id'], 'integer'],
            [['data'], 'safe'],
            [['nome'], 'string', 'max' => 255],
            [['cidade'], 'string', 'max' => 100],
            [['cnpj'], 'string', 'max' => 45],
            [['enquadramento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Enquadramento::className(), 'targetAttribute' => ['enquadramento_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'enquadramento_id' => 'Enquadramento',
            'nome' => 'Nome',
            'cidade' => 'Cidade',
            'cnpj' => 'CNPJ',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnquadramento() {
        return $this->hasOne(Enquadramento::className(), ['id' => 'enquadramento_id']);
    }

}
