<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $empresas = \app\models\Empresa::find()->orderBy('rand()')->all();
        $empresas = $this->renderPartial('//empresa/_list', ['empresas' => $empresas]);
        $enquadr = \app\models\Enquadramento::find()->all();
        return $this->render('index', [
                    'enquadramento' => $enquadr,
                    'empresas' => $empresas
        ]);
    }

    public function actionSearch() {
        $params = Yii::$app->request->post('Empresa');
        $aux = '';
        $query = \app\models\Empresa::find();

        foreach ($params as $key => $param) {
            $query->andFilterWhere(['like', $key, $param]);
        }
        return $this->renderPartial('//empresa/_list', ['empresas' => $query->all()]);
    }

}
