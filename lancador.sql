-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Dez-2019 às 04:09
-- Versão do servidor: 5.7.17
-- versão do PHP: 7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lancador`
--
CREATE DATABASE IF NOT EXISTS `lancador` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lancador`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `enquadramento_id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `cnpj` varchar(45) DEFAULT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `enquadramento_id`, `nome`, `cidade`, `cnpj`, `data`) VALUES
(1, 1, 'Empresa 1', 'Florianópolis', '11.111.111/1111-11', '2019-12-12 00:32:13'),
(2, 2, 'Empresa 2', 'São José', '22.222.222/2222-22', '2019-12-12 00:33:22'),
(3, 3, 'Empresa 3', 'Palhoça', '33.333.333/3333-33', '2019-12-12 02:34:19'),
(4, 5, 'Empresa 4', 'Biguaçu', '44.444.444/4444-44', '2019-12-12 02:07:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enquadramento`
--

DROP TABLE IF EXISTS `enquadramento`;
CREATE TABLE `enquadramento` (
  `id` int(11) NOT NULL,
  `enquadramento` varchar(45) DEFAULT NULL,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `enquadramento`
--

INSERT INTO `enquadramento` (`id`, `enquadramento`, `data`) VALUES
(1, 'A', NULL),
(2, 'B', NULL),
(3, 'C', NULL),
(5, 'D', '2019-12-12 00:43:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`,`enquadramento_id`),
  ADD KEY `fk_empresa_enquadramento_idx` (`enquadramento_id`);

--
-- Indexes for table `enquadramento`
--
ALTER TABLE `enquadramento`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `enquadramento`
--
ALTER TABLE `enquadramento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `fk_empresa_enquadramento` FOREIGN KEY (`enquadramento_id`) REFERENCES `enquadramento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
